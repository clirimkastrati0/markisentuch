<?php include_once 'includes/header.php'; ?>

<div class="header2">
    <div class="overlay">
        <div class="container">
            <div class="inner_text">
                <h1>Über uns</h1>
            </div>
        </div>
    </div>
</div>

<div class="container pt-3 agb-who-is-markisentuch">
    <span class="hr-title"></span> <span class="who-are-markisentuch">Über uns</span>
</div>

<div class="under-header bg-white uberuns">
    <div class="container">
        <div class="row pb-3">
            <div class="col-12 col-sm-12 col-lg-7">
                <div class="pikepyetje-container">
                    <h1 class="header-text pb-2">
                        Markisentuch wurde 1924 gegründet
                    </h1>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-lg-5">
                <p class="right-text">
                    Wir können auf eine lange Firmengeschichte zurückblicken und sind heute stolz darauf, dass wir
                    uns gemeinsam mit unseren Mitarbeitern ständig weiterentwickeln und wachsen.
                </p>
            </div>
        </div>
    </div>
</div>

<div class="questions-section pt-3">
    <div class="container">
        <img src="assets/images/about_1.svg" class="img-fluid" alt="">
        <div class="kontakt-text-container uber_uns-text-container">
            <p class="pt-4">
                Modernster Maschinenpark gepaart mit entsprechend qualifiziertem Personal sind Basis für die
                Fertigung anspruchsvoller Produkte. Mitentscheidend sind jedoch auch Umfeld und Arbeitsklima. Unsere
                ca. 1.300 qm grosse Produktionsfläche wird durch grosse Glasfenster und Glasgiebel lichtdurchflutet
                und bietet auch im grössten Stress ein angenehmes Umfeld. Diese Konzeption wurde konsequent am
                gesamten Gebäude umgesetzt.
                <br><br>
                Um den Marktanforderungen zu entsprechen, wurde im Jahr 1987 die gerollte Fertigung in Verbindung
                mit entsprechendem Versand eingeführt. Dementsprechend sind unsere Zuschneide- und Nähtische sowie
                die Räumlichkeiten des Versands konzipiert.
                <br><br>
                Der Zuschnitt von Acryl erfolgt auf von unten beleuchteten Glastischen, so dass Webfehler
                schnellstmöglich erkannt werden können und nicht im Tuch verarbeitet werden.
                <br><br>
                Wir sind Ausbildungs- und Meisterbetrieb
                <br><br>
                Unser Unternehmen bildet Industriekaufleute und Technische Konfektionäre aus.
            </p>
        </div>
    </div>
</div>

<div class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-lg-5">
                <div class="pr-3">
                    <span class="hr-title uberuns-hr-title"></span>
                    <span class="header-text header-text-uberuns">Über uns</span>
                    <p class="regular-normal uber_uns-upperimage-text pt-2">Firmenhistorie Firma Georg Musculus Gmbh & Co. KG</p>
                    <img src="assets/images/about_2.svg" class="img-fluid about_2_image" alt="">
                    <p class="regular-normal uber_uns-underimage-text pt-3" style="width: 70%;">
                        Wir sind Mitglied im <span style="color: #0060AD;">Industrieverband Technische Textilien – Rollladen – Sonnenschutz
                            e.V.</span>
                    </p>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-lg-7">
                <div class="px-3 div-no-padding-mobile">
                    <p class="regular-normal">
                        Der Großvater des heutigen Geschäftsführers gründete 1924 in Köln die Firma Georg Musculus
                        Zelte-Planen-Markisen. Aus kleinen Anfängen wurde bis zum Zweiten Weltkrieg ein Unternehmen
                        mit ca. 15 Mitarbeitern.
                        <br><br>
                        Das Schicksal vieler Deutscher traf 1944 auch dieses Unternehmen: die totale Vernichtung
                        durch Bombenangriffe. Kurze Zeit später starb Georg Musculus. Sein Sohn fand nach seiner
                        Rückkehr aus russischer Gefangenschaft 1946 nur noch Trümmer vor. Mit großer Energie und
                        tatkräftiger Mithilfe seiner Gattin wurde das Unternehmen an einem neuen Standort wieder
                        aufgebaut. Durch die Fertigung von Camping- und Wohnwagenvorzelten wurde „Musculus“ schon
                        bald über die Grenzen NRW’s hinaus bekannt. <br>
                        Weiteren Expansionen am Nachkriegsstandort Köln-Deutz waren bald Grenzen gesetzt. So wurde
                        das Unternehmen 1980 nach Bensberg verlegt. Hier standen – speziell auf die
                        Firmenbedürfnisse abgestimmt – 3000 m² Produktions- und Verkaufsräume zur Verfügung. Doch
                        diese Gebäude wurden bei einem Großbrand am 02.09.1990 komplett vernichtet. Gemeinsam mit
                        den zwischenzeitlich im Unternehmen tätigen Söhnen Georg Christian und Klaus errichtete der
                        Firmeninhaber Georg Friedrich Musculus innerhalb von 9 Monaten das jetzige Firmengebäude.
                        Nach dessen Fertigstellung zogen sich die Eheleute Musculus aus dem Unternehmen zurück und
                        übergaben die Geschäftsleitung an ihre Söhne.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<hr class="hr-devider">

<div class="container py-2">
    <div class="row last-row">
        <div class="col-12 col-sm-12 col-lg-10">
            <h3 class="subheader-text">Sie sind <span>Fachhändler</span> und an unseren <span>Produkten</span>
                interessiert?</h3>
            <p>Kontaktieren Sie uns, wir beraten Sie gerne:</p>
        </div>
        <div class="col-12 col-sm-12 col-lg-2">
            <button class="leaders-right-about-button">Kontaktiere uns<img src="assets/icons/right_arrow_white-icon.svg" alt=""></button>
        </div>
    </div>
</div>

<hr class="hr-devider">

<?php include_once 'includes/footer.php'; ?>