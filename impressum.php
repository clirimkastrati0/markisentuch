<?php include_once 'includes/header.php'; ?>

<div class="header2">
    <div class="overlay">
        <div class="container">
            <div class="inner_text">
                <h1>Impressum</h1>
            </div>
        </div>
    </div>
</div>

<div class="container pt-3 agb-who-is-markisentuch">
    <span class="hr-title"></span> <span class="who-are-markisentuch">Impressum</span>
</div>

<div class="impressum pt-2 pb-5">
    <div class="container">
        Georg Musculus GmbH & Co. KG
        <br><br>
        Ernst-Reuter-Straße 20, 51427 Bergisch Gladbach
        <br><br>
        Telefon: (02204) 70 49 70 <br>
        Telefax: (02204) 70 49 7-33
        <br><br>
        E-Mail: info@markisentuch.com
        <br><br>
        Umsatzsteueridentifikationsnummer: DE121959936
        <br><br>
        Komplementärin: Georg Musculus Verwaltungs-GmbH HRB 46440
        <br><br>
        Geschäftsführer: Georg Musculus
        <br><br>
        HRG Amtsgericht Köln HRA 19503
        Gerichtsstand: Köln
        <br><br>
        Inhaltlich verantwortlich gem. § 18 Abs. 2 MStV: Sandra Musculus, Christopher Musculus / Georg Musculus GmbH
        & Co. KG / Ernst-Reuter-Straße 20, 51427 Bergisch Gladbach
        <br><br>
        <p>Hier finden Sie unsere <a href="" style="text-decoration: none;"><span style="color: #0060AD; font-weight: bold;">Datenschutz-Erklärung.</span></a></p>
        <p class="pt-2" style="font-weight: bold;">Streitbeteiligungsverfahren</p>
        Wir sind weder verpflichtet noch bereit an Streitbeilegungsverfahren von einer Verbraucherschlichtungsstelle
        teilzunehmen.
        <br>
        <p class="pt-2" style="font-weight: bold;">Urheberrecht</p>
        Alle auf dieser Internetseite veröffentlichten Beiträge, Abbildungen und Daten sind urheberrechtlich
        geschützt. Jede vom Urheberrechtsgesetz nicht zugelassene Verwertung bedarf der vorherigen Zustimmung des
        Herausgebers.
        Dies gilt insbesondere für Vervielfältigung, Bearbeitung, Übersetzung, Einspeicherung, Verarbeitung bzw.
        Wiedergabe von Inhalten in Datenbanken oder anderen elektronischen Medien und Systemen. Fotokopien und
        Downloads von Webseiten dürfen nur für den persönlichen, privaten und nicht kommerziellen Gebrauch
        hergestellt werden.
        Weiterhin können Bilder, Grafiken, Text- oder sonstige Dateien ganz oder teilweise dem Urheberrecht Dritter
        unterliegen. Alle innerhalb des Internetangebotes genannten und ggf. durch Dritte geschützten Marken- und
        Warenzeichen unterliegen uneingeschränkt den Bestimmungen des jeweils gültigen Kennzeichenrechts und den
        Besitzrechten der jeweiligen eingetragenen Eigentümer. Allein aufgrund der bloßen Nennung in unserem
        Internetangebot ist nicht der Schluss zu ziehen, dass Markenzeichen nicht durch Rechte Dritter geschützt
        sind.
        <br>
        <p class="pt-2" style="font-weight: bold;">Haftung</p>
        Alle Informationen und Links werden von uns nach bestem Wissen und Gewissen recherchiert.
        Eine Haftung oder Garantie für die Aktualität, Richtigkeit und Vollständigkeit der Informationen und Daten
        ist ausgeschlossen. Wir behalten uns vor, ohne Ankündigung Änderungen oder Ergänzungen der bereitgestellten
        Informationen oder Daten vorzunehmen.
        <br><br>
        Haftungshinweis: Trotz sorgfältiger inhaltlicher Kontrolle übernehmen wir keine Haftung für die Inhalte
        externer Links. Für den Inhalt der verlinkten Seiten sind ausschließlich deren Betreiber verantwortlich.
        <br><br>
        Credits: <span style="color: #0060AD; font-weight: bold;">Proudly powered by WordPress</span>
        <br>
    </div>
</div>

<hr class="hr-devider">

<?php include_once 'includes/footer.php'; ?>