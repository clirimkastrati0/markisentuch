<?php include_once 'includes/header.php'; ?>

<div class="header">
    <div class="overlay">
        <div class="container">
            <div class="inner_text">
                <h1>Manufaktur für technische textilien und Markisentücher</h1>
                <p>Hier finden sie einen Überblick über unser Lieferprogramm für Fachhändler. Kollektionen und
                    Preislisten senden wir Fachhändlern auf Anfrage gerne zu.</p>
                <button type="button" class="btn btn-primary">Zur aktuellen Kollektion</button>
            </div>
            <img class="bottm-arrow-icon" src="assets/icons/botom_arrow-icon.svg" alt="" srcset="">
        </div>
    </div>
</div>
<!-- Discover Section -->
<div class="discover-section pt-5">
    <div class="container" id="getContainerMargin">
        <div class="row">
            <div class="col-sm-12 col-md-8">
                <h1 class="header-text">Entdecken Sie einzigartige Sammlungen</h1>
            </div>
            <div class="col-sm-12 col-md-4">
                <form id="landing-search-form" action="">
                    <div class="input-group pb-3">
                        <input type="text" class="form-control shadow-none" placeholder="Artikelnummer">
                        <div class="input-group-append">
                            <button type="submit" class="input-group-text"><img src="assets/icons/search-icon.svg" alt=""></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <hr class="header-line">
    </div>
    <div class="owl-carousel-container">
        <div class="owl-carousel">
            <div class="card">
                <img class="card-img-top" src="assets/images/carousel1.svg" alt="">
                <div class="card-body">
                    <h5 class="card-title">Acrylic</h5>
                    <p class="card-text">An interweaving of yarns, textures and colours. 100% solution-dyed
                        acrylic (polyacrylic)</p>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top" src="assets/images/carousel2.svg" alt="">
                <div class="card-body">
                    <h5 class="card-title">Polyester</h5>
                    <p class="card-text">An interweaving of yarns, textures and colours. 100% solution-dyed
                        acrylic (polyacrylic)</p>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top" src="assets/images/carousel3.svg" alt="">
                <div class="card-body">
                    <h5 class="card-title">Soltis</h5>
                    <p class="card-text">An interweaving of yarns, textures and colours. 100% solution-dyed
                        acrylic (polyacrylic)</p>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top" src="assets/images/carousel4.svg" alt="">
                <div class="card-body">
                    <h5 class="card-title">Screen</h5>
                    <p class="card-text">An interweaving of yarns, textures and colours. 100% solution-dyed
                        acrylic (polyacrylic)</p>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top" src="assets/images/carousel5.svg" alt="">
                <div class="card-body">
                    <h5 class="card-title">Speedtest Star Screen</h5>
                    <p class="card-text">An interweaving of yarns, textures and colours. 100% solution-dyed
                        acrylic (polyacrylic)</p>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top" src="assets/images/carousel1.svg" alt="">
                <div class="card-body">
                    <h5 class="card-title">Saddler Twilight</h5>
                    <p class="card-text">An interweaving of yarns, textures and colours. 100% solution-dyed
                        acrylic (polyacrylic)</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Leaders Section -->
<div class="leaders-section section-padding">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-6">
                <br><br>
                <span class="hr-title"></span> <span class="who-are-markisentuch">Wer sind Markisentuch?</span>
                <h1 class="header-text mt-3">Führend in Textil seit 1924</h1>
                <div class="leaders-left-container">
                    <img src="assets/images/leaders-left-image.svg" class="img-fluid leaders-left-right-image leaders-right-imageee" alt="">
                    <h2 class="subheader-text py-1">Wir geben Produktion aus jedem Blickwinkel</h2>
                    <p class="leaders-right-description" style="padding-top: 5px;padding-right: 50px;">
                        1924 gegründet blicken wir auf eine lange Unternehmenshistorie zurück und sind heute stolz
                        darauf, uns stetig weiter zu entwickeln und gemeinsam mit unseren Mitarbeitern zu wachsen.
                    </p>
                    <button class="leaders-right-about-button">Über Uns<img src="assets/icons/right_arrow_white-icon.svg" alt=""></button>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-6">
                <div class="leaders-right-container">
                    <img src="assets/images/leaders-right-image.svg" class="img-fluid leaders-left-right-image leaders-right-imagee" alt="">
                    <h1 class="header-text">Eine Kombination aus Tradition und Moderne</h1>
                    <br>
                    <p class="leaders-right-description">
                        Utilizing our expert knowledge in polyester sun awning fabrics, we have
                        specially developed
                        the bespoke Freedom Collections for the European market. This new trailblazing
                        collection is
                        setting us apart with surprising colour combinations and stylish designs. It is
                        a real
                        game-changer for the way you experience shade. Making it more colourful and
                        atmospheric than
                        ever before! <br>
                        <span style=" display: inline-block; padding-top: 25px;">
                            You can choose a fabric which differs in colour on either side of the
                            fabric. The result
                            being a beautiful neutral appearance on the outside with an impressive light
                            experience on
                            the inside.
                        </span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Produkte section -->
<div class="produkte pb-5" id="Produkte" name="Produkte">
    <img src="assets/images/produkte-image.png" alt="">
    <div class="box bg-white top-right">
        <h1>Produkte</h1>
        <p>
            Wir fertigen in Bergisch Gladbach eine Vielzahl von Produkten aus unterschiedlichen Materialien auf
            Natur- und Synthetikbasis. <br> <br>

            Vom klassischen Markisentuch über den Volant und das Seitenteil, Balkontücher nach Skizze oder
            Sichtblenden – Sonnenschutztextilien sind unser Schwerpunkt. <br> <br>

            Erweitert wird dieses Angebot um Planen, Schutzhauben, Werbebanner, Schirmbespannungen, Arbeitsschirme,
            Transportttaschen, Dächer für Verkaufsstände, Bootspersenninge und nahe zu jegliche Sonderkonfektion aus
            technischen Textilien, die unsere Kunden wünschen. <br> <br>

            Unser moderner Maschinenpark ermöglicht es uns, die verschiedensten Gewebe fachgerecht zu verarbeiten um
            eine hohe Qualität und lange Lebensdauer der Produkte zu gewährleisten. <br> <br>

            Unsere qualifizierten Mitarbeiter kombinieren Handwerk und Technik, um alle Produkte nach
            Kundenanforderung fachgerecht zu fertigen. <br>
            Selbstverständlich stehen wir auch für Reparaturen und Lohnkonfektion zur Verfügung.

        </p>
    </div>
</div>

<!-- Sonderkonfektion section -->
<div class="sonderkonfektion pb-5">
    <img src="assets/images/sonderkonfektion-image.png" alt="">
    <div class="clearfix"></div>
    <div class="box bg-white top-left p-5">
        <h1>Sonderkonfektion</h1>
        <p>
            Manchmal passt eine Standard-Lösung einfach nicht und es bedarf einer Sonderlösung. <br>
            Unsere Mitarbeiter beraten Sie gerne dazu, ohne das technisch Umsetzbare aus den Augen zu verlieren.
            <br>
            Ob mehrfarbige Tücher, Logo-Beschriftung oder bedruckbare Materialien – vieles ist möglich und kann
            individuell gefertigt werden. <br>
            Und natürlich sind auch Zuschnitte und Tücher in Sonderformen umsetzbar, hierzu bedarf es oftmals nur
            einer Skizze und einer sauberen technischen Absprache und Umsetzung.

        </p>
    </div>
</div>

<!-- Couponservice section -->
<div class="couponservice pb-5" id="Couponservice" name="Couponservice">
    <img src="assets/images/couponservice-image.png" alt="">
    <div class="box bg-white top-right p-5">
        <br>
        <h1>Couponservice</h1>
        <p class="pt-3">
            <span>Coupon: <span style="color: #0060AD;">gerollt (Meterware)</span> </span> <br>
            Alle Qualitäten unserer Hauskollektion bieten wir Ihnen auch im Coupon-Service.
            Bei unifarbenen Dessins und bei Blockstreifen behalten wir uns bei Farbgleichheit die Wahl des
            Lieferanten vor. Bitte fordern Sie die erhältlichen Qualitäten sowie die Couponpreisliste
            an: info@markisentuch.com
        </p>
        <br><br>
    </div>
</div>

<div class="container py-2">
    <div class="row last-row">
        <div class="col-12 col-sm-12 col-lg-10">
            <h3 class="subheader-text">Sie sind <span>Fachhändler</span> und an unseren <span>Produkten</span>
                interessiert?</h3>
            <p>Kontaktieren Sie uns, wir beraten Sie gerne:</p>
        </div>
        <div class="col-12 col-sm-12 col-lg-2">
            <button class="leaders-right-about-button">Kontaktiere uns<img src="assets/icons/right_arrow_white-icon.svg" alt=""></button>
        </div>
    </div>
</div>
<div class="container">
    <hr class="hr-devider">
</div>

<?php include_once 'includes/footer.php'; ?>
