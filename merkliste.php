<?php include_once 'includes/header.php'; ?>

<div class="under-header pt-3 pb-5 mb-5">
    <div class="container">
        <div class="row pt-4">
            <div class="col-7 col-md-8 col-lg-9">
                <div class="header-text-black merkliste-header">Merkliste</div>
            </div>
            <div class="col-5 col-md-4 col-lg-3">
                <p class="merkliste-produkte-number"><span>6</span> Produkte</p>
            </div>
        </div>
        <div class="container">
            <hr class="hr-devider">
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <div class="wishlist-item">
                    <img src="assets/images/color_1.svg" alt="">
                    <div class="color-description">
                        <p class="category-name">Polyester</p>
                        <p class="color-number">315420</p>
                    </div>
                    <span class="remove-wishlist-item"><img src="assets/icons/remove-wishlist-item-icon.svg" class="img-fluid w-50" alt=""></span>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="wishlist-item">
                    <img src="assets/images/color_2.svg" alt="">
                    <div class="color-description">
                        <p class="category-name">Acryl</p>
                        <p class="color-number">315420</p>
                    </div>
                    <span class="remove-wishlist-item"><img src="assets/icons/remove-wishlist-item-icon.svg" class="img-fluid w-50" alt=""></span>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="wishlist-item">
                    <img src="assets/images/color_3.svg" alt="">
                    <div class="color-description">
                        <p class="category-name">Acryl</p>
                        <p class="color-number">315420</p>
                    </div>
                    <span class="remove-wishlist-item"><img src="assets/icons/remove-wishlist-item-icon.svg" class="img-fluid w-50" alt=""></span>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="wishlist-item">
                    <img src="assets/images/color_2.svg" alt="">
                    <div class="color-description">
                        <p class="category-name">Acryl</p>
                        <p class="color-number">315420</p>
                    </div>
                    <span class="remove-wishlist-item"><img src="assets/icons/remove-wishlist-item-icon.svg" class="img-fluid w-50" alt=""></span>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="wishlist-item">
                    <img src="assets/images/color_3.svg" alt="">
                    <div class="color-description">
                        <p class="category-name">Acryl</p>
                        <p class="color-number">315420</p>
                    </div>
                    <span class="remove-wishlist-item"><img src="assets/icons/remove-wishlist-item-icon.svg" class="img-fluid w-50" alt=""></span>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="wishlist-item">
                    <img src="assets/images/color_1.svg" alt="">
                    <div class="color-description">
                        <p class="category-name">Polyester</p>
                        <p class="color-number">315420</p>
                    </div>
                    <span class="remove-wishlist-item"><img src="assets/icons/remove-wishlist-item-icon.svg" class="img-fluid w-50" alt=""></span>
                </div>
            </div>
        </div>
    </div>
</div>

<hr class="hr-devider">

<?php include_once 'includes/footer.php'; ?>