    <!-- Footer Section -->
    <div class="footer">
        <div class="logo-container mt-3">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-4 offset-md-4">
                        <img src="assets/images/GM_Logo 3.png" class="img-fluid" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-row-1 py-4">
            <div class="container px-0">
                <div class="row px-0 mx-0">
                    <div class="col-6 col-sm-6 col-md-3 px-0 mx-0">
                        <ul class="px-0">
                            <li> <a href="index.php#Produkte">Produkte </a></li>
                            <li> <a href="index.php#Couponservice">Coupon Service</a></li>
                        </ul>
                    </div>
                    <div class="col-6 col-sm-6 col-md-3">
                        <ul>
                            <li> <a href="dessins.php">Dessins</a></li>
                            <li> <a href="kontakt.php">Kontakt</a></li>
                        </ul>
                    </div>
                    <div class="col-6 col-sm-6 col-md-3">
                        <ul>
                            <li> <a href="uber_uns.php">Über uns</a></li>
                            <li> <a href="verstärkung_gesucht.php">Verstärkung gesucht</a></li>
                        </ul>
                    </div>
                    <div class="col-6 col-sm-6 col-md-3">
                        <ul>
                            <li> <a href="faq.php">FAQ</a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-row-2 py-3">
            <div class="container px-0">
                <div class="row px-0 mx-0">
                    <div class="col-sm-12 col-md-3 px-0 mx-0">
                        <div class="d-flex">
                            <div class="flex-item">
                                <img src="assets/icons/phone-icon.svg" class="img-fluid" alt="">
                            </div>
                            <div class="flex-item">
                                +49 (0) 2204 70 49 70
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="d-flex">
                            <div class="flex-item">
                                <img src="assets/icons/location-icon.svg" class="img-fluid" alt="">
                            </div>
                            <div class="flex-item">
                                Ernst-Reuter-Straße 20-22
                                51427 Bergisch Gladbach
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-3">
                        <div>
                            <a target="_blank" href="https://www.facebook.com"><img src="assets/icons/facebook-icon.svg" alt=""></a>
                            <a target="_blank" href="https://www.instagram.com"><img src="assets/icons/instagram-icon.svg" alt=""></a>
                            <a target="_blank" href="https://www.youtube.com"><img src="assets/icons/youtube-icon.svg" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-row-3 py-2">
            <div class="container px-0">
                <div class="row px-0 mx-0">
                    <div class="col-sm-12 col-md-5 px-0 mx-0">
                        <ul class="px-0 mx-0">
                            <li> <a href="">© Georg Musculus GmbH & Co. KG </a></li>
                        </ul>
                    </div>
                    <div class="col-sm-12 col-md-7 pb-0">
                        <ul>
                            <li> <a href="impressum.php"> Impressum</a></li>
                            <li> <a href="agb.php"> AGB</a></li>
                            <li> <a href="dsgvo.php"> DSGVO</a></li>
                            <li> <a href="datenschutz.php"> Datenschutz</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="assets/plugins/jquery3.6/jquery-3.6.0.min.js"></script>
    <script src="assets/plugins/popper/popper.min.js"></script>
    <script src="assets/plugins/owlcarousel2/dist/owl.carousel.min.js"></script>
    <script src="assets/js/main.js"></script>
    </body>

    </html>