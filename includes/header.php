<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="icon" href="assets/images/logo/GM_Logo_Tuecher_Insignie.png">
    <link rel="stylesheet" href="assets/plugins/bootstrap5/css/bootstrap.min.css">
    <script src="assets/plugins/bootstrap5/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="assets/plugins/owlcarousel2/dist/assets/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/plugins/owlcarousel2/dist/assets/owl.theme.default.min.css">
    <link rel="stylesheet" href="style.css">
    <title>Markisentuch</title>
</head>

<body id="body">
    <nav class="navbar navbar-expand-md bg-white navbar-light menus sticky-top">
        <div class="container">
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#nav-content" aria-controls="nav-content" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <a class="navbar-brand" href="index.php"><img src="assets/images/logo/GM_Logo 2.png" class="img-fluid"></a>
            <div class="collapse navbar-collapse justify-content-end" id="nav-content">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link p active-menu" href="index.php#Produkte">Produkte</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link c" href="index.php#Couponservice">Coupon service</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link d" href="dessins.php">Dessins</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link k" href="kontakt.php">Kontakt</a>
                    </li>
                </ul>
                <span class="search_card_container">
                    <div class="dropdown">
                        <a class="dropdown-merkliste-btn" type="button">
                            <img src="assets/icons/shopping_bag-icon.svg" alt="No Image">
                            <span class="badge position-absolute left-100 translate-middle">3</span>
                        </a>
                        <ul class="merkliste-dropdown dropdown-menu" aria-labelledby="navbarDropdown">
                            <div class="arrow-up"></div>
                            <div>
                                <div class="row px-3 pt-2">
                                    <div class="col-6">
                                        <h1 class="merkliste-dropdown-header">Merkliste</h1>
                                    </div>
                                    <div class="col-6">
                                        <p class="merkliste-dropdown-header-number">6 Produkte</p>
                                    </div>
                                    <hr class="hr-devider">
                                </div>
                                <div class="merkliste-dropdown-container px-3">
                                    <div class="wishlist-item wishlist-item-dropdown">
                                        <img src="assets/images/color_1.svg" class="wishlist-image-dropdownn" alt="">
                                        <div class="color-description">
                                            <p class="category-name-merkliste">Acryl</p>
                                            <p class="color-number-merkliste">315420</p>
                                        </div>
                                        <span class="remove-wishlist-item remove-image-wishlist">
                                            <img src="assets/icons/remove-wishlist-item-icon.svg" class="img-fluid w-50" alt="">
                                        </span>
                                    </div>
                                    <div class="wishlist-item wishlist-item-dropdown">
                                        <img src="assets/images/color_2.svg" class="wishlist-image-dropdownn" alt="">
                                        <div class="color-description">
                                            <p class="category-name-merkliste">Acryl</p>
                                            <p class="color-number-merkliste">315420</p>
                                        </div>
                                        <span class="remove-wishlist-item remove-image-wishlist">
                                            <img src="assets/icons/remove-wishlist-item-icon.svg" class="img-fluid w-50" alt="">
                                        </span>
                                    </div>
                                    <div class="wishlist-item wishlist-item-dropdown">
                                        <img src="assets/images/color_3.svg" class="wishlist-image-dropdownn" alt="">
                                        <div class="color-description">
                                            <p class="category-name-merkliste">Acryl</p>
                                            <p class="color-number-merkliste">315420</p>
                                        </div>
                                        <span class="remove-wishlist-item remove-image-wishlist">
                                            <img src="assets/icons/remove-wishlist-item-icon.svg" class="img-fluid w-50" alt="">
                                        </span>
                                    </div>
                                </div>
                                <div class="row my-3">
                                    <div class="col-12">
                                        <button onclick="window.location.href='merkliste.php'" class="view-more-merkliste">Merkliste anzeigen</button>
                                    </div>
                                </div>
                            </div>
                        </ul>
                    </div>
                </span>
            </div>
        </div>
    </nav>