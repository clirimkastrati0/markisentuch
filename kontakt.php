<?php include_once 'includes/header.php'; ?>

<div class="header2">
    <div class="overlay">
        <div class="container">
            <div class="inner_text">
                <h1>Kontakt</h1>
            </div>
        </div>
    </div>
</div>

<div class="under-header pt-3">
    <div class="container">
        <div class="row pb-3">
            <div class="col-sm-12 col-lg-5 pt-4">
                <span class="hr-title"></span>
                <span class="who-are-markisentuch">Kontaktiere uns
                </span>
                <div class="pikepyetje-container kontakt-text-container">
                    <h1 class="header-text">
                        Danke für Ihr Interesse an Markisentuch
                    </h1>
                    <p class="pt-4">
                        Markisentuch ist hier, um Ihnen bei allen Fragen oder Anliegen zu helfen. Wir beraten Sie
                        gerne.
                        <br><br>
                        Bei uns gelistete Fachhändler können schnell und einfach per Mail oder Fax bei uns
                        bestellen.
                        Gerne können Sie dazu folgende Formulare verwenden:
                        <br><br>
                        <span>Bestellblatt Markisentuch</span> (pdf download link)* <br>
                        <span>Skizze Balkontuch</span> (pdf download link)* <br>
                        <span>Skizze Schrägtuch</span> (pdf download link)*
                        <br><br>
                        Sie wünschen Kontakt zu unserer Qualitätssicherung? <br>
                        Wir streben danach, Ihnen immer ein einwandfreies Produkt zu liefern. <br>
                        Sollte es dennoch zu einer Reklamation kommen, füllen Sie bitte das folgende Formular aus
                        und
                        senden es mit aussagekräftigem Bildmaterial an <span>info@markisentuch.com</span> <br>
                        <span>Fragebogen Qualitätssicherung</span> (pdf download link)*
                        <br><br>
                        (Bitte speichern Sie die Formulare lokal ab und öffnen die Datei mit einem geeigneten Reader
                        und
                        nicht im Browser. Danke!)
                    </p>
                </div>
            </div>
            <div class="col-sm-12 col-lg-7 mt-4">
                <h3 class="subheader-text kontakt-subheader">
                    Ihr direkter Kontakt zu unserer Sachbearbeitung:
                </h3>
                <div class="row">
                    <div class="col-sm-12 col-lg-6">
                        <div class="contact-box">
                            <div>
                                <img src="assets/icons/contact-name-icon.svg" alt="">
                                <span>Michael K</span>
                            </div>
                            <div>
                                <img src="assets/icons/contact-phone-icon.svg" alt="">
                                <span>02204 70 49 7-24</span>
                            </div>
                            <div>
                                <img src="assets/icons/contact-email-icon.svg" alt="">
                                <span class="email-text">michael_kirchenmayer@markisentuch.com</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-6">
                        <div class="contact-box">
                            <div>
                                <img src="assets/icons/contact-name-icon.svg" alt="">
                                <span>Michael K</span>
                            </div>
                            <div>
                                <img src="assets/icons/contact-phone-icon.svg" alt="">
                                <span>02204 70 49 7-24</span>
                            </div>
                            <div>
                                <img src="assets/icons/contact-email-icon.svg" alt="">
                                <span class="email-text">michael_kirchenmayer@markisentuch.com</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-6">
                        <div class="contact-box">
                            <div>
                                <img src="assets/icons/contact-name-icon.svg" alt="">
                                <span>Michael K</span>
                            </div>
                            <div>
                                <img src="assets/icons/contact-phone-icon.svg" alt="">
                                <span>02204 70 49 7-24</span>
                            </div>
                            <div>
                                <img src="assets/icons/contact-email-icon.svg" alt="">
                                <span class="email-text">michael_kirchenmayer@markisentuch.com</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-lg-6">
                        <div class="contact-box">
                            <div>
                                <img src="assets/icons/contact-name-icon.svg" alt="">
                                <span>Michael K</span>
                            </div>
                            <div>
                                <img src="assets/icons/contact-phone-icon.svg" alt="">
                                <span>02204 70 49 7-24</span>
                            </div>
                            <div>
                                <img src="assets/icons/contact-email-icon.svg" alt="">
                                <span class="email-text">michael_kirchenmayer@markisentuch.com</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<hr class="hr-devider">

<?php include_once 'includes/footer.php'; ?>