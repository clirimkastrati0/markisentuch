<?php include_once 'includes/header.php'; ?>

<div class="header2">
    <div class="overlay">
        <div class="container">
            <div class="inner_text">
                <h1>FAQ</h1>
            </div>
        </div>
    </div>
</div>

<div class="container pt-3 agb-who-is-markisentuch">
    <span class="hr-title"></span> <span class="who-are-markisentuch">Frequently Asked Questions</span>
</div>

<div class="under-header bg-white">
    <div class="container">
        <div class="row pb-3">
            <div class="col-12 col-sm-12 col-lg-7">
                <div class="pikepyetje-container">
                    <img src="assets/icons/faq-icon.svg" alt="" class="faq-icon img-fluid">
                    <h1 class="header-text-black">
                        Fragen?
                        <span class="header-text">Antworten finden Sie hier</span>
                    </h1>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-lg-5">
                <p class="right-text">
                    Finden Sie Antworten auf häufig gestellte Fragen in den Produktinformationen. Wenn Sie nicht
                    finden,
                    wonach Sie suchen, kontaktieren Sie uns und wir können Ihnen helfen.
                </p>
            </div>
        </div>
    </div>
</div>

<hr class="hr-devider">

<div class="questions-section section-padding">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-lg-5">
                <div class="questions">
                    <div class="active" question="1">
                        <p>
                            <span>01.</span> Wo finde ich die „Richtlinie zur Beurteilung von konfektionierten
                            Markisentüchern“?
                        </p>
                    </div>
                    <div question="2">
                        <p>
                            <span>02.</span> Was bedeutet „spinndüsengefärbt“?
                        </p>
                    </div>
                    <div question="3">
                        <p>
                            <span>03.</span> Welche Eigenschaften haben Markisentücher aus spinndüsengefärbtem
                            Polyester?
                        </p>
                    </div>
                    <div question="4">
                        <p>
                            <span>04.</span> Erfüllt Acryl die REACH-Verordnung?
                        </p>
                    </div>
                    <div question="5">
                        <p>
                            <span>05.</span> Wie erhalte ich Zertifikate oder technische Daten?
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-lg-7">
                <div class="answer">
                    <div answer="1">
                        <h2>
                            <span>01.</span>
                            Wo finde ich die „Richtlinie zur Beurteilung von konfektionierten
                            Markisentüchern“?
                        </h2>
                        <p>
                            Die“Richtlinie zur Beurteilung von konfektionierten Markisentüchern“ des
                            Industrieverband Technische Textilien – Rollladen – Sonnenschutz e.V. finden sie auf der
                            Homepage des ITRS e.V. zum Download. Hier geht es direkt zum
                            <span>Downloadbereich.</span>
                        </p>
                    </div>
                    <div answer="2" style="display: none;">
                        <h2>
                            <span>02.</span>
                            Was bedeutet „spinndüsengefärbt“?
                        </h2>
                        <p>
                            Die“Richtlinie zur Beurteilung von konfektionierten Markisentüchern“ des
                            Industrieverband Technische Textilien – Rollladen – Sonnenschutz e.V. finden sie auf der
                            Homepage des ITRS e.V. zum Download. Hier geht es direkt zum
                            <span>Downloadbereich.</span>
                        </p>
                    </div>
                    <div answer="3" style="display: none;">
                        <h2>
                            <span>03.</span>
                            Welche Eigenschaften haben Markisentücher aus spinndüsengefärbtem Polyester?
                        </h2>
                        <p>
                            Die“Richtlinie zur Beurteilung von konfektionierten Markisentüchern“ des
                            Industrieverband Technische Textilien – Rollladen – Sonnenschutz e.V. finden sie auf der
                            Homepage des ITRS e.V. zum Download. Hier geht es direkt zum
                            <span>Downloadbereich.</span>
                        </p>
                    </div>
                    <div answer="4" style="display: none;">
                        <h2>
                            <span>04.</span>
                            Erfüllt Acryl die REACH-Verordnung?
                        </h2>
                        <p>
                            Die“Richtlinie zur Beurteilung von konfektionierten Markisentüchern“ des
                            Industrieverband Technische Textilien – Rollladen – Sonnenschutz e.V. finden sie auf der
                            Homepage des ITRS e.V. zum Download. Hier geht es direkt zum
                            <span>Downloadbereich.</span>
                        </p>
                    </div>
                    <div answer="5" style="display: none;">
                        <h2>
                            <span>05.</span>
                            Wie erhalte ich Zertifikate oder technische Daten?
                        </h2>
                        <p>
                            Die“Richtlinie zur Beurteilung von konfektionierten Markisentüchern“ des
                            Industrieverband Technische Textilien – Rollladen – Sonnenschutz e.V. finden sie auf der
                            Homepage des ITRS e.V. zum Download. Hier geht es direkt zum
                            <span>Downloadbereich.</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<hr class="hr-devider">

<div class="container py-2">
    <div class="row last-row">
        <div class="col-12 col-sm-12 col-lg-10">
            <h3 class="subheader-text">Sie sind <span>Fachhändler</span> und an unseren <span>Produkten</span>
                interessiert?</h3>
            <p>Kontaktieren Sie uns, wir beraten Sie gerne:</p>
        </div>
        <div class="col-12 col-sm-12 col-lg-2">
            <button class="leaders-right-about-button">Kontaktiere uns<img src="assets/icons/right_arrow_white-icon.svg" alt=""></button>
        </div>
    </div>
</div>

<hr class="hr-devider">

<?php include_once 'includes/footer.php'; ?>