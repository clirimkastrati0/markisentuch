<?php include_once 'includes/header.php'; ?>

<div class="header2">
    <div class="overlay">
        <div class="container">
            <div class="inner_text">
                <h1>AGB</h1>
            </div>
        </div>
    </div>
</div>
<div class="container pt-3 agb-who-is-markisentuch">
    <span class="hr-title"></span> <span class="who-are-markisentuch">AGB</span>
</div>
<div class="pt-2">
    <div class="container pb-5">
        <h2 class="subheader-text agb-header">
            Hier finden Sie unsere Allgemeinen Geschäfts- und Lieferbedingungen zum Download:
        </h2>
        <button class="leaders-right-about-button mt-4 agb-download-button">AGB</button>
    </div>
</div>

<?php include_once 'includes/footer.php'; ?>