$(document).ready(function () {
  // Home page
  // Carousel(slider)
  $(".owl-carousel").owlCarousel({
    loop: true,
    autoplay: true,
    slideTransition: "linear",
    autoplayTimeout: 2000,
    margin: 10,
    responsiveBaseElement: 'body',
    nav: false,
    dots: false,
    responsive: {
      0: {
        items: 2,
      },
      500: {
        items: 3,
      },
      768: {
        items: 4,
      },
      1000: {
        items: 6,
      },
    },
  });

  // FAQ page
  // Questions and answer
  $(".questions div").on("click", function () {
    $(".questions div").removeClass("active");
    $(this).addClass("active");
    $("[answer]").hide();
    $('[answer="' + $(this).attr("question") + '"]').show();
  });

  // Colors Page
  $(".small-color-card").on("click", function () {
    $(".small-color-card").removeClass("active-color");
    $(this).addClass("active-color");
    $(".big-image").attr("src", $(this).find("img").attr("src"));
  });
  $(".filter-box div").on("click", function () {
    $(".filter-box div").removeClass("active");
    $(this).addClass("active");
  });

  $('.front-image').on('click', function () {
    $(".big-image").attr("src", $(this).attr("src"));
  })
  $('.back-image').on('click', function () {
    $(".big-image").attr("src", $(this).attr("src"));
  })

  $(".dropdown-acryl-btn").click(function () {
    $(".acryl-dropdown.dropdown-menu").toggleClass("show");
  });
  $(".dropdown-soltis-btn").click(function () {
    $(".soltis-dropdown.dropdown-menu").toggleClass("show");
  });
  $(".dropdown-merkliste-btn").click(function () {
    $(".merkliste-dropdown.dropdown-menu").toggleClass("show");
  });

  if (window.location.href.indexOf("index") > -1) {

    var element = document.getElementById("getContainerMargin").currentStyle || window.getComputedStyle(document.getElementById("getContainerMargin"));
    var containerMarginRight = element.marginRight.replace('px', '');
    var containerMarginLeft = element.marginLeft.replace('px', '');

    var bodyElement = document.getElementById("body").currentStyle || window.getComputedStyle(document.getElementById("body"));
    var screenSize = bodyElement.width.replace('px', '');

    if (parseInt(screenSize) > 0 && parseInt(screenSize) <= 1024) {

      $('.owl-carousel-container').attr('style', 'margin-left: 0px !important');

      $('.produkte').addClass('container');
      $('.produkte').attr('style', 'width: 100% !important');
      $('.produkte img').attr('style', 'width: 100% !important');

      $('.sonderkonfektion').addClass('container');
      $('.sonderkonfektion').attr('style', 'width: 100% !important');
      $('.sonderkonfektion img').attr('style', 'width: 100% !important');

      $('.couponservice').addClass('container');
      $('.couponservice').attr('style', 'width: 100% !important');
      $('.couponservice img').attr('style', 'width: 100% !important');

    } else if (parseInt(screenSize) > 1024) {

      $('.owl-carousel-container').attr('style', 'margin-left:' + (parseInt(containerMarginLeft) + 10) + 'px !important');
      $('.sonderkonfektion img').attr('style', 'width: 100% !important');
      $('.sonderkonfektion').attr('style', 'margin-left:' + (parseInt(containerMarginLeft) + 130) + 'px !important');

      $('.couponservice img').attr('style', 'width: 100% !important');
      $('.couponservice').attr('style', 'margin-right:' + (parseInt(containerMarginRight) + 130) + 'px !important');

      $('.produkte img').attr('style', 'width: 100% !important');
      $('.produkte').attr('style', 'margin-right:' + (parseInt(containerMarginRight) + 130) + 'px !important');

    }
  }

  $('.nav-link').on('click', function () {
    $('.nav-link').removeClass('active-menu')
    $(this).addClass('active-menu')
  })

  $.each($('.nav-link'), function () {
    $(this).removeClass('active-menu');
    if (window.location.href.indexOf('dessins') > - 1) {
      $('.d').addClass('active-menu')
    } else if (window.location.href.indexOf('Produkte') > - 1) {
      $('.p').addClass('active-menu')
    } else if (window.location.href.indexOf('Couponservice') > - 1) {
      $('.c').addClass('active-menu')
    } if (window.location.href.indexOf('kontakt') > - 1) {
      $('.k').addClass('active-menu')
    }
  })

  //add BT DD show event
  $(".dropdown").on("click", function() {
    var $btnDropDown = $(this).find(".dropdown-toggle");
    var $listHolder = $(this).find(".dropdown-menu");
    //reset position property for DD container
    $(this).attr("style", "position: static !important");
    $listHolder.css({
      "top": ($btnDropDown.offset().top + $btnDropDown.outerHeight(true)) + "px",
      "left": $btnDropDown.offset().left + "px"
    });
    $listHolder.data("open", true);
  });
  //add BT DD hide event
  $(".dropdown").on("hide", function() {
    var $listHolder = $(this).find(".dropdown-menu");
    $listHolder.data("open", false);
  });
  //add on scroll for table holder
  $(".filter-box").scroll(function() {
    var $ddHolder = $(this).find(".dropdown")
    var $btnDropDown = $(this).find(".dropdown-toggle");
    var $listHolder = $(this).find(".dropdown-menu");
    if ($listHolder.data("open")) {
      $listHolder.css({
        "top": ($btnDropDown.offset().top + $btnDropDown.outerHeight(true)) + "px",
        "left": $btnDropDown.offset().left + "px"
      });
      $ddHolder.toggleClass("open", ($btnDropDown.offset().left > $(this).offset().left))
    }
  })
});

