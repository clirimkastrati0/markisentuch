<?php include_once 'includes/header.php'; ?>

<div class="header2">
    <div class="overlay">
        <div class="container">
            <div class="inner_text">
                <h1>Datenschutz</h1>
            </div>
        </div>
    </div>
</div>

<div class="datenschutz py-3 pb-5">
    <div class="container">
        Datenschutzerklärung nach DSGVO
        <br><br>
        Name und Anschrift des Verantwortlichen <br>
        Der Verantwortliche im Sinne der Datenschutz-Grundverordnung und anderer nationaler
        <br><br>
        Datenschutzgesetze der Mitgliedsstaaten sowie sonstiger datenschutzrechtlicher Bestimmungen ist die:
        <br><br>
        Georg Musculus GmbH & Co. KG
        <br><br>
        Ernst-Reuter-Straße 20-22 51427
        <br><br>
        Bergisch Gladbach Deutschland
        <br><br>
        Tel.: 02204/70 49 70
        <br><br>
        E-Mail: info@markisentuch.com
        <br><br>
        Website: www.markisentuch.com
        <br><br>
        Name und Anschrift des Datenschutzbeauftragten
        Der Datenschutzbeauftragte des Verantwortlichen ist:
        <br><br>
        Heiko Deitz
        <br><br>
        DeDaCo (Deitz Datenschutz Consulting)
        <br><br>
        Steinenkamp 20 51469 Bergisch Gladbach
        <br><br>
        Deutschland
        <br><br>
        Tel.: 02202/9275880
        <br><br>
        E-Mail: info@dedaco.de
        <br><br>
        Website: www.dedaco.de
        <br><br>
        III. Allgemeines zur Datenverarbeitung
        <br><br>
        Umfang der Verarbeitung personenbezogener Daten
        Wir erheben und verwenden personenbezogene Daten unserer Nutzer grundsätzlich nur, soweit dies zur
        Bereitstellung einer funktionsfähigen Website sowie unserer Inhalte und Leistungen erforderlich ist.
        <br><br>
        Die Erhebung und Verwendung personenbezogener Daten unserer Nutzer erfolgt regelmäßig nur nach Einwilligung
        des Nutzers. Eine Ausnahme gilt in solchen Fällen, in denen eine vorherige Einholung einer Einwilligung aus
        tatsächlichen Gründen nicht möglich ist und die Verarbeitung der Daten durch gesetzliche Vorschriften
        gestattet ist.
        <br><br>
        Rechtsgrundlage für die Verarbeitung personenbezogener Daten
        Soweit wir für Verarbeitungsvorgänge personenbezogener Daten eine Einwilligung der betroffenen Person
        einholen, dient Art. 6 Abs. 1 lit. a EU-Datenschutzgrundverordnung (DSGVO) als Rechtsgrundlage für die
        Verarbeitung personenbezogener Daten.
        <br><br>
        Bei der Verarbeitung von personenbezogenen Daten, die zur Erfüllung eines Vertrages, dessen Vertragspartei
        die betroffene Person ist, erforderlich ist, dient Art. 6 Abs. 1 lit. b DSGVO als Rechtsgrundlage.
        <br><br>
        Dies gilt auch für Verarbeitungsvorgänge, die zur Durchführung vorvertraglicher Maßnahmen erforderlich sind.
        <br><br>
        Soweit eine Verarbeitung personenbezogener Daten zur Erfüllung einer rechtlichen Verpflichtung erforderlich
        ist, der unser Unternehmen unterliegt, dient Art. 6 Abs. 1 lit. c DSGVO als Rechtsgrundlage.
        <br><br>
        Für den Fall, dass lebenswichtige Interessen der betroffenen Person oder einer anderen natürlichen Person
        eine Verarbeitung personenbezogener Daten erforderlich machen, dient Art. 6 Abs. 1 lit. d DSGVO als
        Rechtsgrundlage.
        <br><br>
        Ist die Verarbeitung zur Wahrung eines berechtigten Interesses unseres Unternehmens oder eines Dritten
        erforderlich und überwiegen die Interessen, Grundrechte und Grundfreiheiten des Betroffenen das erstgenannte
        Interesse nicht, so dient Art. 6 Abs. 1 lit. f DSGVO als Rechtsgrundlage für die Verarbeitung.
        <br><br>
        Datenlöschung und Speicherdauer
        Die personenbezogenen Daten der betroffenen Person werden gelöscht oder gesperrt, sobald der Zweck der
        Speicherung entfällt. Eine Speicherung kann darüber hinaus dann erfolgen, wenn dies durch den europäischen
        oder nationalen Gesetzgeber in unionsrechtlichen Verordnungen, Gesetzen oder sonstigen Vorschriften, denen
        der Verantwortliche unterliegt, vorgesehen wurde.
        <br><br>
        Eine Sperrung oder Löschung der Daten erfolgt auch dann, wenn eine durch die genannten Normen
        vorgeschriebene Speicherfrist abläuft, es sei denn, dass eine Erforderlichkeit zur weiteren Speicherung der
        Daten für einen Vertragsabschluss oder eine Vertragserfüllung besteht.
        <br><br>
        IV. Bereitstellung der Website und Erstellung von Logfiles
        <br><br>
        Beschreibung und Umfang der Datenverarbeitung
        Bei jedem Aufruf unserer Internetseite erfasst unser System automatisiert Daten und Informationen vom
        Computersystem des aufrufenden Rechners.
        <br><br>
        Folgende Daten werden hierbei erhoben:
        <br><br>
        (1) Informationen über den Browsertyp und die verwendete Version
        <br><br>
        (2) Das Betriebssystem des Nutzers
        <br><br>
        (3) Den Internet-Service-Provider des Nutzers
        <br><br>
        (4) Die IP-Adresse des Nutzers
        <br><br>
        (5) Datum und Uhrzeit des Zugriffs
        <br><br>
        (6) Websites, von denen das System des Nutzers auf unsere Internetseite gelangt
        <br><br>
        (7) Websites, die vom System des Nutzers über unsere Website aufgerufen werden
        <br><br>
        Die Daten werden ebenfalls in den Logfiles unseres Systems gespeichert. Eine Speicherung dieser Daten
        zusammen mit anderen personenbezogenen Daten des Nutzers findet nicht statt.
        <br><br>
        Rechtsgrundlage für die Datenverarbeitung
        Rechtsgrundlage für die vorübergehende Speicherung der Daten und der Logfiles ist Art. 6 Abs. 1 lit. f
        DSGVO.
        <br><br>
        Zweck der Datenverarbeitung
        Die vorübergehende Speicherung der IP-Adresse durch das System ist notwendig, um eine Auslieferung der
        Website an den Rechner des Nutzers zu ermöglichen. Hierfür muss die IP-Adresse des Nutzers für die Dauer der
        Sitzung gespeichert bleiben. Die Speicherung in Logfiles erfolgt, um die Funktionsfähigkeit der Website
        sicherzustellen. Zudem dienen uns die Daten zur Optimierung der Website und zur Sicherstellung der
        Sicherheit unserer informationstechnischen Systeme.
        <br><br>
        Eine Auswertung der Daten zu Marketingzwecken findet in diesem Zusammenhang nicht statt. In diesen Zwecken
        liegt auch unser berechtigtes Interesse an der Datenverarbeitung nach Art. 6 Abs. 1 lit. f DSGVO.
        <br><br>
        Dauer der Speicherung
        Die Daten werden gelöscht, sobald sie für die Erreichung des Zweckes ihrer Erhebung nicht mehr erforderlich
        sind. Im Falle der Erfassung der Daten zur Bereitstellung der Website ist dies der Fall, wenn die jeweilige
        Sitzung beendet ist.
        <br><br>
        Im Falle der Speicherung der Daten in Logfiles ist dies nach spätestens sieben Tagen der Fall. Eine
        darüberhinausgehende Speicherung ist möglich. In diesem Fall werden die IP-Adressen der Nutzer gelöscht oder
        verfremdet, sodass eine Zuordnung des aufrufenden Clients nicht mehr möglich ist.
        <br><br>
        Widerspruchs- und Beseitigungsmöglichkeit
        Die Erfassung der Daten zur Bereitstellung der Website und die Speicherung der Daten in Logfiles ist für den
        Betrieb der Internetseite zwingend erforderlich.
        <br><br>
        Es besteht folglich seitens des Nutzers keine Widerspruchsmöglichkeit.
        <br><br>
        V. Consent-Management-Platform
        <br><br>
        Beschreibung und Umfang der Datenverarbeitung
        Eine Consent Management Platform (kurz CMP) ist eine Software, um eine datenschutzrechtliche Einwilligung
        der Besucher auf Webseiten, Apps usw. einzuholen und zu speichern, bevor Nutzerdaten über Website-Skripte
        erfasst werden (Tracking).
        <br><br>
        Rechtsgrundlage für die Datenverarbeitung
        Rechtsgrundlage für die Verarbeitung dieser Daten ist Art. 6 Abs. 1 lit. f) DSGVO (berechtigte Interessen
        von uns als Verantwortliche).
        <br><br>
        Zweck der Datenverarbeitung
        Die Verarbeitung dieser personenbezogenen Daten dient uns die Zustimmung für die Nutzung von Cookies
        abzufragen, zu dokumentieren und zu verwalten
        <br><br>
        Dauer der Speicherung
        Die Speicherdauer der Cookies entnehmen Sie bitte der Consent-Management-Platform.
        <br><br>
        Widerspruchsmöglichkeit
        Sie können Ihr Recht auf Einwilligung jederzeit widerrufen und nicht notwendige Cookies deaktivieren.
        <br><br>
        VI. Verwendung von Cookies
        <br><br>
        a) Beschreibung und Umfang der Datenverarbeitung
        Unsere Webeseite verwendet Cookies.
        <br><br>
        Bei Cookies handelt es sich um Textdateien, die im Internetbrowser bzw. vom Internetbrowser auf dem
        Computersystem des Nutzers gespeichert werden. Ruft ein Nutzer eine Website auf, so kann ein Cookie auf dem
        Betriebssystem des Nutzers gespeichert werden. Dieser Cookie enthält eine charakteristische Zeichenfolge,
        die eine eindeutige Identifizierung des Browsers beim erneuten Aufrufen der Website ermöglicht.
        <br><br>
        Wir setzen Cookies ein, um unsere Website nutzerfreundlicher zu gestalten. Einige Elemente unserer
        Internetseite erfordern es, dass der aufrufende Browser auch nach einem Seitenwechsel identifiziert werden
        kann.
        <br><br>
        In den Cookies werden dabei folgende Daten gespeichert und übermittelt:
        <br><br>
        (1) Log-In-Informationen
        <br><br>
        Wir verwenden auf unserer Website darüber hinaus Cookies, die eine Analyse des Surfverhaltens der Nutzer
        ermöglichen.
        <br><br>
        Link zum Tool: https://de.borlabs.io/borlabs-cookie/
        <br><br>
        Auf diese Weise können folgende Daten übermittelt werden:
        <br><br>
        (1) Eingegebene Suchbegriffe
        <br><br>
        (2) Häufigkeit von Seitenaufrufen
        <br><br>
        (3) Inanspruchnahme von Website-Funktionen
        <br><br>
        Die auf diese Weise erhobenen Daten der Nutzer werden durch technische Vorkehrungen pseudonymisiert.
        <br><br>
        Daher ist eine Zuordnung der Daten zum aufrufenden Nutzer nicht mehr möglich. Die Daten werden nicht
        gemeinsam mit sonstigen personenbezogenen Daten der Nutzer gespeichert.
        <br><br>
        Beim Aufruf unserer Website werden die Nutzer durch einen Infobanner über die Verwendung von Cookies zu
        Analysezwecken informiert und auf diese Datenschutzerklärung verwiesen.
        <br><br>
        b) Rechtsgrundlage für die Datenverarbeitung
        Die Rechtsgrundlage für die Verarbeitung personenbezogener Daten unter Verwendung technisch notwendiger
        Cookies ist Art. 6 Abs. 1 lit. f DSGVO.
        <br><br>
        Die Rechtsgrundlage für die Verarbeitung personenbezogener Daten unter Verwendung von Cookies zu
        Analysezwecken ist bei Vorliegen einer diesbezüglichen Einwilligung des Nutzers Art. 6 Abs. 1 lit. a DSGVO.
        <br><br>
        c) Zweck der Datenverarbeitung
        Der Zweck der Verwendung technisch notwendiger Cookies ist, die Nutzung von Websites für die Nutzer zu
        vereinfachen. Einige Funktionen unserer Internetseite können ohne den Einsatz von Cookies nicht angeboten
        werden. Für diese ist es erforderlich, dass der Browser auch nach einem Seitenwechsel wiedererkannt wird.
        Für folgende Anwendungen benötigen wir Cookies:
        <br><br>
        (1) Merken von Suchbegriffen
        <br><br>
        Die durch technisch notwendige Cookies erhobenen Nutzerdaten werden nicht zur Erstellung von Nutzerprofilen
        verwendet.
        <br><br>
        Die Verwendung der Analyse-Cookies erfolgt zu dem Zweck, die Qualität unserer Website und ihre Inhalte zu
        verbessern. Durch die Analyse-Cookies erfahren wir, wie die Website genutzt wird und können so unser Angebot
        stetig optimieren. In diesen Zwecken liegt auch unser berechtigtes Interesse in der Verarbeitung der
        personenbezogenen Daten nach Art. 6 Abs. 1 lit. f DSGVO.
        <br><br>
        d) Dauer der Speicherung, Widerspruchs- und Beseitigungsmöglichkeit
        Cookies werden auf dem Rechner des Nutzers gespeichert und von diesem an unserer Seite übermittelt. Daher
        haben Sie als Nutzer auch die volle Kontrolle über die Verwendung von Cookies. Durch eine Änderung der
        Einstellungen in Ihrem Internetbrowser können Sie die Übertragung von Cookies deaktivieren oder
        einschränken. Bereits gespeicherte Cookies können jederzeit gelöscht werden. Dies kann auch automatisiert
        erfolgen. Werden Cookies für unsere Website deaktiviert, können möglicherweise nicht mehr alle Funktionen
        der Website vollumfänglich genutzt werden.
        <br><br>
        e) Third-Party-Cookies
        Einbindung von Diensten und Inhalten Dritter
        <br><br>
        Es kann vorkommen, dass innerhalb dieses Onlineangebotes Inhalte Dritter, wie zum Beispiel Videos von
        YouTube, Kartenmaterial von Google-Maps, RSS-Feeds oder Grafiken von anderen Webseiten eingebunden werden.
        Dies setzt immer voraus, dass die Anbieter dieser Inhalte (nachfolgend bezeichnet als “Dritt-Anbieter”) die
        IP-Adresse der Nutzer wahrnehmen. Denn ohne die IP-Adresse könnten sie die Inhalte nicht an den Browser des
        jeweiligen Nutzers senden. Die IP-Adresse ist damit für die Darstellung dieser Inhalte erforderlich. Wir
        bemühen uns nur solche Inhalte zu verwenden, deren jeweilige Anbieter die IP-Adresse lediglich zur
        Auslieferung der Inhalte verwenden. Jedoch haben wir keinen Einfluss darauf, falls die Dritt-Anbieter die
        IP-Adresse z.B. für statistische Zwecke speichern.
        <br><br>
        Soweit dies uns bekannt ist, klären wir die Nutzer darüber auf.
        <br><br>
        Eine Verhinderungsmöglichkeit zur Speicherung dieser Cookies findet sich in den Einstellungen des Browsers.
        Rechtsgrundlage für die Verwendung von Third-Party-Cookies ist Art. 6 Abs. 1 lit. f DSGVO.
        <br><br>
        VII. Soziale Medien: Onlinepräsenzen
        <br><br>
        Facebook-Fanpages
        <br><br>
        Beschreibung und Umfang der Datenverarbeitung
        Wir sind Betreiber einer/von Facebook Fanpage/s innerhalb des sozialen Netzwerks Facebook, um mit den dort
        aktiven Kunden, Interessenten und Nutzern kommunizieren und sie dort über unsere Artikel bzw. Leistungen
        informieren zu können.
        <br><br>
        Wir weisen darauf hin, dass dabei Daten der Nutzer außerhalb des Raumes der Europäischen Union verarbeitet
        werden können. Hierdurch können sich für die Nutzer Risiken ergeben, weil so z.B. die Durchsetzung der
        Rechte der Nutzer erschwert werden könnte. Ferner werden die Daten der Nutzer im Regelfall für
        Marktforschungs- und Werbezwecke verarbeitet. So können z.B. aus dem Nutzungsverhalten und sich daraus
        ergebenden Interessen der Nutzer Nutzungsprofile erstellt werden.
        <br><br>
        Die Nutzungsprofile können wiederum verwendet werden, um z.B. Werbeanzeigen innerhalb und außerhalb der
        Plattformen zu schalten, die mutmaßlich den Interessen der Nutzer entsprechen. Zu diesen Zwecken werden im
        Regelfall Cookies auf den Rechnern der Nutzer gespeichert, in denen das Nutzungsverhalten und die Interessen
        der Nutzer gespeichert werden.
        <br><br>
        Ferner können in den Nutzungsprofilen auch Daten unabhängig der von den Nutzern verwendeten Geräte
        gespeichert werden (insbesondere, wenn die Nutzer Mitglieder der jeweiligen Plattformen sind und bei diesen
        eingeloggt sind).
        <br><br>
        Rechtsgrundlage für die Datenverarbeitung
        Die Verarbeitung der personenbezogenen Daten der Nutzer erfolgt auf Grundlage unserer berechtigten
        Interessen an einer effektiven Information der Nutzer und Kommunikation/Interaktion mit den Nutzern gem.
        Art. 6 Abs. 1 lit. f. DSGVO.
        <br><br>
        Falls die Nutzer von den jeweiligen Anbietern der Plattformen um eine Einwilligung, z.B. mit Hilfe einer
        Checkbox in die vorbeschriebene Datenverarbeitung gebeten werden, ist die Rechtsgrundlage der Verarbeitung
        Art. 6 Abs. 1 lit. a., Art. 7 DSGVO. c.
        <br><br>
        Weitere Informationen zur Datenverarbeitung
        <br><br>
        Für eine detaillierte Darstellung der jeweiligen Verarbeitungen und der Widerspruchsmöglichkeiten (Opt-Out),
        verweisen wir auf die nachfolgend verlinkten Angaben der Anbieter. Auch im Fall von Auskunftsanfragen und
        der Geltendmachung von Nutzerrechten, weisen wir darauf hin, dass diese am effektivsten bei den Anbietern
        geltend gemacht werden können. Nur die Anbieter haben jeweils Zugriff auf die Daten der Nutzer und können
        direkt entsprechende Maßnahmen ergreifen und Auskünfte geben.
        <br><br>
        Sollten Sie dennoch Hilfe benötigen, dann können Sie sich an uns wenden.
        <br><br>
        Opt-Out: www.facebook.com/settings?tab=ads oder www.youronlinechoices.com/de/
        <br><br>
        Wir betreiben zudem folgende Social-Media-Onlinepräsenzen:
        <br><br>
        Facebook, Google, Twitter, Xing, LinkedIn, YouTube.
        <br><br>
        Ihre personenbezogenen Daten werden von dem jeweiligen Anbieter (bei US-amerikanischen Anbietern in den USA)
        verarbeitet.
        <br><br>
        Der Anbieter verarbeitet die über Sie erhobenen Daten als Nutzungsprofile und nutzt diese für Zwecke der
        Werbung, Marktforschung und/oder bedarfsgerechten Gestaltung seiner Website. Eine solche Auswertung erfolgt
        insbesondere zur Darstellung von bedarfsgerechter Werbung. Der Anbieter nimmt die Datenerhebung insbesondere
        über Cookies vor. Über die Betreibung von Social-Media-Onlinepräsenzen bieten wir Ihnen die Möglichkeit, mit
        den sozialen Netzwerken und anderen Nutzern zu interagieren, so dass wir unser Angebot verbessern und für
        Sie als Nutzer interessanter ausgestalten können.
        <br><br>
        Rechtsgrundlage für das Betreiben der Social-Media-Onlinepräsenzen ist Art. 6 Abs. 1 S. 1 lit. f DSGVO.
        <br><br>
        Um Ihre Rechte als Betroffene Person geltend zu machen, wenden Sie sich am besten direkt an den jeweiligen
        Anbieter. Sollten Sie dazu Unterstützung benötigen, helfen wir Ihnen gerne.
        <br><br>
        Weitere Informationen zu Zweck und Umfang der Datenerhebung, zur Verarbeitung und zur Nutzung des
        Widerspruchsrechts erhalten Sie in den folgenden Datenschutzerklärungen des jeweiligen Anbieters. Dort
        erhalten Sie auch weitere Informationen zu Ihren diesbezüglichen Rechten und Einstellungsmöglichkeiten zum
        Schutze Ihrer Privatsphäre.
        <br><br>
        Adressen der jeweiligen Anbieter und URL mit deren Datenschutzhinweisen:
        <br><br>
        1.a) Facebook Ireland Ltd.4 Grand Canal Square Grand Canal Harbour Dublin 2, Ireland
        <br><br>
        1.b) Google Ireland Limited, Gordon House, Barrow Street, Dublin 4, Irland
        <br><br>
        1.c) Twitter, Inc., 1355 Market St, Suite 900, San Francisco, California 94103, USA;
        https://twitter.com/privacy. Twitter hat sich dem EU-US-Privacy-Shield unterworfen,
        https://www.privacyshield.gov/EU-US-Framework.
        <br><br>
        1.d) xing AG, Gänsemarkt 43, 20354 Hamburg, DE; http://www.xing.com/privacy.
        <br><br>
        1.e) LinkedIn Corporation, 2029 Stierlin Court, Mountain View, California 94043, USA;
        http://www.linkedin.com/legal/privacy-policy
        <br><br>
        1.f) Youtube, gehört zur Google Ireland Limited, Gordon House, 4 Barrow St, Dublin, D04 E5W5, Irland;
        https://www.google.com/policies/privacy/partners/?hl=de
        <br><br>
        VIII. Google Fonts
        <br><br>
        Beschreibung und Umfang der Datenverarbeitung
        Diese Seite nutzt zur einheitlichen Darstellung von Schriftarten so genannte Web Fonts, die von Google
        bereitgestellt werden. Beim Aufruf einer Seite lädt Ihr Browser die benötigten Web Fonts in ihren
        Browsercache, um Texte und Schriftarten korrekt anzuzeigen. Zu diesem Zweck muss der von Ihnen verwendete
        Browser Verbindung zu den Servern von Google aufnehmen. Hierdurch erlangt Google Kenntnis darüber, dass über
        Ihre IP-Adresse unsere Website aufgerufen wurde.
        <br><br>
        Wenn Ihr Browser Web Fonts nicht unterstützt, wird eine Standardschrift von Ihrem Computer genutzt.
        <br><br>
        Rechtsgrundlage für die Datenverarbeitung
        Rechtsgrundlage für die Einbindung dieser Schriften ins Theme ist Art. 6 Abs. 1 lit. f DSGVO.
        <br><br>
        Zweck der Datenverarbeitung
        Die Nutzung von Google Web Fonts erfolgt im Interesse einer einheitlichen und ansprechenden Darstellung
        unserer Online-Angebote. Keine Gültigkeit mehr In diesen Zwecken liegt auch unser berechtigtes Interesse an
        der Datenverarbeitung nach Art. 6 Abs. 1 lit. f DSGVO.
        <br><br>
        Dauer der Speicherung
        Die Daten werden gelöscht, sobald sie für die Erreichung des Zweckes ihrer Erhebung nicht mehr erforderlich
        sind.
        <br><br>
        Widerspruchs- und Beseitigungsmöglichkeit
        Web Fonts werden im Browsercache des Nutzers gespeichert. Daher haben Sie als Nutzer auch die volle
        Kontrolle über die Verwendung von Web Fonts. Durch eine Änderung der Einstellungen in Ihrem Internetbrowser
        können Sie die Übertragung von Web Fonts deaktivieren oder einschränken.
        <br><br>
        IXVIII. Rechte der betroffenen Person
        <br><br>
        Werden personenbezogene Daten von Ihnen verarbeitet, sind Sie Betroffener i.S.d. DSGVO und es stehen Ihnen
        folgende Rechte gegenüber dem Verantwortlichen zu:
        <br><br>
        Auskunftsrecht Sie können von dem Verantwortlichen eine Bestätigung darüber verlangen, ob personenbezogene
        Daten, die Sie betreffen, von uns verarbeitet werden. Liegt eine solche Verarbeitung vor, können Sie von dem
        Verantwortlichen über folgende Informationen Auskunft verlangen:
        (1) die Zwecke, zu denen die personenbezogenen Daten verarbeitet werden;
        <br><br>
        (2) die Kategorien von personenbezogenen Daten, welche verarbeitet werden;
        <br><br>
        (3) die Empfänger bzw. die Kategorien von Empfängern, gegenüber denen die Sie betreffenden personenbezogenen
        Daten offengelegt wurden oder noch offengelegt werden;
        <br><br>
        (4) die geplante Dauer der Speicherung der Sie betreffenden personenbezogenen Daten oder, falls konkrete
        Angaben hierzu nicht möglich sind, Kriterien für die Festlegung der Speicherdauer;
        <br><br>
        (5) das Bestehen eines Rechts auf Berichtigung oder Löschung der Sie betreffenden personenbezogenen Daten,
        eines Rechts auf Einschränkung der Verarbeitung durch den Verantwortlichen oder eines Widerspruchsrechts
        gegen diese Verarbeitung;
        <br><br>
        (6) das Bestehen eines Beschwerderechts bei einer Aufsichtsbehörde;
        <br><br>
        (7) alle verfügbaren Informationen über die Herkunft der Daten, wenn die personenbezogenen Daten nicht bei
        der betroffenen Person erhoben werden;
        <br><br>
        (8) das Bestehen einer automatisierten Entscheidungsfindung einschließlich Profiling gemäß Art. 22 Abs. 1
        und 4 DSGVO und – zumindest in diesen Fällen – aussagekräftige Informationen über die involvierte Logik
        sowie die Tragweite und die angestrebten Auswirkungen einer derartigen Verarbeitung für die betroffene
        Person.
        <br><br>
        Ihnen steht das Recht zu, Auskunft darüber zu verlangen, ob die Sie betreffenden personenbezogenen Daten in
        ein Drittland oder an eine internationale Organisation übermittelt werden. In diesem Zusammenhang können Sie
        verlangen, über die geeigneten Garantien gem. Art. 46 DSGVO im Zusammenhang mit der Übermittlung
        unterrichtet zu werden.
        <br><br>
        Recht auf Berichtigung
        Sie haben ein Recht auf Berichtigung und/oder Vervollständigung gegenüber dem Verantwortlichen, sofern die
        verarbeiteten personenbezogenen Daten, die Sie betreffen, unrichtig oder unvollständig sind. Der
        Verantwortliche hat die Berichtigung unverzüglich vorzunehmen.
        <br><br>
        Recht auf Einschränkung der Verarbeitung
        Unter den folgenden Voraussetzungen können Sie die Einschränkung der Verarbeitung der Sie betreffenden
        personenbezogenen Daten verlangen:
        <br><br>
        (1) wenn Sie die Richtigkeit der Sie betreffenden personenbezogenen für eine Dauer bestreiten, die es dem
        Verantwortlichen ermöglicht, die Richtigkeit der personenbezogenen Daten zu überprüfen;
        <br><br>
        (2) die Verarbeitung unrechtmäßig ist und Sie die Löschung der personenbezogenen Daten ablehnen und
        stattdessen die Einschränkung der Nutzung der personenbezogenen Daten verlangen;
        <br><br>
        (3) der Verantwortliche die personenbezogenen Daten für die Zwecke der Verarbeitung nicht länger benötigt,
        Sie diese jedoch zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen benötigen, oder
        <br><br>
        (4) wenn Sie Widerspruch gegen die Verarbeitung gemäß Art. 21 Abs. 1 DSGVO eingelegt haben und noch nicht
        feststeht, ob die berechtigten Gründe des Verantwortlichen gegenüber Ihren Gründen überwiegen.
        <br><br>
        Wurde die Verarbeitung der Sie betreffenden personenbezogenen Daten eingeschränkt, dürfen diese Daten – von
        ihrer Speicherung abgesehen – nur mit Ihrer Einwilligung oder zur Geltendmachung, Ausübung oder Verteidigung
        von Rechtsansprüchen oder zum Schutz der Rechte einer anderen natürlichen oder juristischen Person oder aus
        Gründen eines wichtigen öffentlichen Interesses der Union oder eines Mitgliedstaats verarbeitet werden.
        <br><br>
        Wurde die Einschränkung der Verarbeitung nach den o.g. Voraussetzungen eingeschränkt, werden Sie von dem
        Verantwortlichen unterrichtet bevor die Einschränkung aufgehoben wird.
        <br><br>
        Recht auf Löschung
        a) Löschungspflicht
        <br><br>
        Sie können von dem Verantwortlichen verlangen, dass die Sie betreffenden personenbezogenen Daten
        unverzüglich gelöscht werden, und der Verantwortliche ist verpflichtet, diese Daten unverzüglich zu löschen,
        sofern einer der folgenden Gründe zutrifft:
        <br><br>
        (1) Die Sie betreffenden personenbezogenen Daten sind für die Zwecke, für die sie erhoben oder auf sonstige
        Weise verarbeitet wurden, nicht mehr notwendig.
        <br><br>
        (2) Sie widerrufen Ihre Einwilligung, auf die sich die Verarbeitung gem. Art. 6 Abs. 1 lit. a oder Art. 9
        Abs. 2 lit. a DSGVO stützte, und es fehlt an einer anderweitigen Rechtsgrundlage für die Verarbeitung.
        <br><br>
        (3) Sie legen gem. Art. 21 Abs. 1 DSGVO Widerspruch gegen die Verarbeitung ein und es liegen keine
        vorrangigen berechtigten Gründe für die Verarbeitung vor, oder Sie legen gem. Art. 21 Abs. 2 DSGVO
        Widerspruch gegen die Verarbeitung ein.
        <br><br>
        (4) Die Sie betreffenden personenbezogenen Daten wurden unrechtmäßig verarbeitet.
        <br><br>
        (5) Die Löschung der Sie betreffenden personenbezogenen Daten ist zur Erfüllung einer rechtlichen
        Verpflichtung nach dem Unionsrecht oder dem Recht der Mitgliedstaaten erforderlich, dem der Verantwortliche
        unterliegt.
        <br><br>
        (6) Die Sie betreffenden personenbezogenen Daten wurden in Bezug auf angebotene Dienste der
        Informationsgesellschaft gemäß Art. 8 Abs. 1 DSGVO erhoben.
        <br><br>
        b) Information an Dritte
        <br><br>
        Hat der Verantwortliche die Sie betreffenden personenbezogenen Daten öffentlich gemacht und ist er gem. Art.
        17 Abs. 1 DSGVO zu deren Löschung verpflichtet, so trifft er unter Berücksichtigung der verfügbaren
        Technologie und der Implementierungskosten angemessene Maßnahmen, auch technischer Art, um für die
        Datenverarbeitung Verantwortliche, die die personenbezogenen Daten verarbeiten, darüber zu informieren, dass
        Sie als betroffene Person von ihnen die Löschung aller Links zu diesen personenbezogenen Daten oder von
        Kopien oder Replikationen dieser personenbezogenen Daten verlangt haben.
        <br><br>
        c) Ausnahmen
        <br><br>
        Das Recht auf Löschung besteht nicht, soweit die Verarbeitung erforderlich ist
        <br><br>
        (1) zur Ausübung des Rechts auf freie Meinungsäußerung und Information;
        <br><br>
        (2) zur Erfüllung einer rechtlichen Verpflichtung, die die Verarbeitung nach dem Recht der Union oder der
        Mitgliedstaaten, dem der Verantwortliche unterliegt, erfordert, oder zur Wahrnehmung einer Aufgabe, die im
        öffentlichen Interesse liegt oder in Ausübung öffentlicher Gewalt erfolgt, die dem Verantwortlichen
        übertragen wurde;
        <br><br>
        (3) aus Gründen des öffentlichen Interesses im Bereich der öffentlichen Gesundheit gemäß Art. 9 Abs. 2 lit.
        h und i sowie Art. 9 Abs. 3 DSGVO;
        <br><br>
        (4) für im öffentlichen Interesse liegende Archivzwecke, wissenschaftliche oder historische Forschungszwecke
        oder für statistische Zwecke gem. Art. 89 Abs. 1 DSGVO, soweit das unter Abschnitt a) genannte Recht
        voraussichtlich die Verwirklichung der Ziele dieser Verarbeitung unmöglich macht oder ernsthaft
        beeinträchtigt, oder
        <br><br>
        (5) zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen.
        <br><br>
        Recht auf Unterrichtung
        Haben Sie das Recht auf Berichtigung, Löschung oder Einschränkung der Verarbeitung gegenüber dem
        Verantwortlichen geltend gemacht, ist dieser verpflichtet, allen Empfängern, denen die Sie betreffenden
        personenbezogenen Daten offengelegt wurden, diese Berichtigung oder Löschung der Daten oder Einschränkung
        der Verarbeitung mitzuteilen, es sei denn, dies erweist sich als unmöglich oder ist mit einem
        unverhältnismäßigen Aufwand verbunden.
        <br><br>
        Ihnen steht gegenüber dem Verantwortlichen das Recht zu, über diese Empfänger unterrichtet zu werden.
        <br><br>
        Recht auf Datenübertragbarkeit
        Sie haben das Recht, die Sie betreffenden personenbezogenen Daten, die Sie dem Verantwortlichen
        bereitgestellt haben, in einem strukturierten, gängigen und maschinenlesbaren Format zu erhalten. Außerdem
        haben Sie das Recht diese Daten einem anderen Verantwortlichen ohne Behinderung durch den Verantwortlichen,
        dem die personenbezogenen Daten bereitgestellt wurden, zu übermitteln, sofern
        <br><br>
        (1) die Verarbeitung auf einer Einwilligung gem. Art. 6 Abs. 1 lit. a DSGVO oder Art. 9 Abs. 2 lit. a DSGVO
        oder auf einem Vertrag gem. Art. 6 Abs. 1 lit. b DSGVO beruht und
        <br><br>
        (2) die Verarbeitung mithilfe automatisierter Verfahren erfolgt.
        <br><br>
        In Ausübung dieses Rechts haben Sie ferner das Recht, zu erwirken, dass die Sie betreffenden
        personenbezogenen Daten direkt von einem Verantwortlichen einem anderen Verantwortlichen übermittelt werden,
        soweit dies technisch machbar ist. Freiheiten und Rechte anderer Personen dürfen hierdurch nicht
        beeinträchtigt werden.
        <br><br>
        Das Recht auf Datenübertragbarkeit gilt nicht für eine Verarbeitung personenbezogener Daten, die für die
        Wahrnehmung einer Aufgabe erforderlich ist, die im öffentlichen Interesse liegt oder in Ausübung
        öffentlicher Gewalt erfolgt, die dem Verantwortlichen übertragen wurde.
        <br><br>
        Widerspruchsrecht
        Sie haben das Recht, aus Gründen, die sich aus ihrer besonderen Situation ergeben, jederzeit gegen die
        Verarbeitung der Sie betreffenden personenbezogenen Daten, die aufgrund von Art. 6 Abs. 1 lit. e oder f
        DSGVO erfolgt, Widerspruch einzulegen; dies gilt auch für ein auf diese Bestimmungen gestütztes Profiling.
        <br><br>
        Der Verantwortliche verarbeitet die Sie betreffenden personenbezogenen Daten nicht mehr, es sei denn, er
        kann zwingende schutzwürdige Gründe für die Verarbeitung nachweisen, die Ihre Interessen, Rechte und
        Freiheiten überwiegen, oder die Verarbeitung dient der Geltendmachung, Ausübung oder Verteidigung von
        Rechtsansprüchen.
        <br><br>
        Werden die Sie betreffenden personenbezogenen Daten verarbeitet, um Direktwerbung zu betreiben, haben Sie
        das Recht, jederzeit Widerspruch gegen die Verarbeitung der Sie betreffenden personenbezogenen Daten zum
        Zwecke derartiger Werbung einzulegen; dies gilt auch für das Profiling, soweit es mit solcher Direktwerbung
        in Verbindung steht.
        <br><br>
        Widersprechen Sie der Verarbeitung für Zwecke der Direktwerbung, so werden die Sie betreffenden
        personenbezogenen Daten nicht mehr für diese Zwecke verarbeitet.
        <br><br>
        Sie haben die Möglichkeit, im Zusammenhang mit der Nutzung von Diensten der Informationsgesellschaft –
        ungeachtet der Richtlinie 2002/58/EG – Ihr Widerspruchsrecht mittels automatisierter Verfahren auszuüben,
        bei denen technische Spezifikationen verwendet werden.
        <br><br>
        Recht auf Widerruf der datenschutzrechtlichen Einwilligungserklärung
        Sie haben das Recht, Ihre datenschutzrechtliche Einwilligungserklärung jederzeit zu widerrufen. Durch den
        Widerruf der Einwilligung wird die Rechtmäßigkeit der aufgrund der Einwilligung bis zum Widerruf erfolgten
        Verarbeitung nicht berührt.
        <br><br>
        Automatisierte Entscheidung im Einzelfall einschließlich Profiling
        Sie haben das Recht, nicht einer ausschließlich auf einer automatisierten Verarbeitung – einschließlich
        Profiling – beruhenden Entscheidung unterworfen zu werden, die Ihnen gegenüber rechtliche Wirkung entfaltet
        oder Sie in ähnlicher Weise erheblich beeinträchtigt. Dies gilt nicht, wenn die Entscheidung
        <br><br>
        (1) für den Abschluss oder die Erfüllung eines Vertrags zwischen Ihnen und dem Verantwortlichen erforderlich
        ist,
        <br><br>
        (2) aufgrund von Rechtsvorschriften der Union oder der Mitgliedstaaten, denen der Verantwortliche
        unterliegt, zulässig ist und diese Rechtsvorschriften angemessene Maßnahmen zur Wahrung Ihrer Rechte und
        Freiheiten sowie Ihrer berechtigten Interessen enthalten oder
        <br><br>
        (3) mit Ihrer ausdrücklichen Einwilligung erfolgt. Allerdings dürfen diese Entscheidungen nicht auf
        besonderen Kategorien personenbezogener Daten nach Art. 9 Abs. 1 DSGVO beruhen, sofern nicht Art. 9 Abs. 2
        lit. a oder g gilt und angemessene Maßnahmen zum Schutz der Rechte und Freiheiten sowie Ihrer berechtigten
        Interessen getroffen wurden.
        <br><br>
        Hinsichtlich der in (1) und (3) genannten Fälle trifft der Verantwortliche angemessene Maßnahmen, um die
        Rechte und Freiheiten sowie Ihre berechtigten Interessen zu wahren, wozu mindestens das Recht auf Erwirkung
        des Eingreifens einer Person seitens des Verantwortlichen, auf Darlegung des eigenen Standpunkts und auf
        Anfechtung der Entscheidung gehört.
        <br><br>
        Recht auf Beschwerde bei einer Aufsichtsbehörde
        Unbeschadet eines anderweitigen verwaltungsrechtlichen oder gerichtlichen Rechtsbehelfs steht Ihnen das
        Recht auf Beschwerde bei einer Aufsichtsbehörde, insbesondere in dem Mitgliedstaat ihres Aufenthaltsorts,
        ihres Arbeitsplatzes oder des Orts des mutmaßlichen Verstoßes, zu, wenn Sie der Ansicht sind, dass die
        Verarbeitung der Sie betreffenden personenbezogenen Daten gegen die DSGVO verstößt.
        <br><br>
        Die Aufsichtsbehörde, bei der die Beschwerde eingereicht wurde, unterrichtet den Beschwerdeführer über den
        Stand und die Ergebnisse der Beschwerde einschließlich der Möglichkeit eines gerichtlichen Rechtsbehelfs
        nach Art. 78 DSGVO.
    </div>
</div>

<hr class="hr-devider">

<div class="container py-2">
    <div class="row last-row">
        <div class="col-12 col-sm-12 col-lg-10">
            <h3 class="subheader-text">Sie sind <span>Fachhändler</span> und an unseren <span>Produkten</span>
                interessiert?</h3>
            <p>Kontaktieren Sie uns, wir beraten Sie gerne:</p>
        </div>
        <div class="col-12 col-sm-12 col-lg-2">
            <button class="leaders-right-about-button">Kontaktiere uns<img src="assets/icons/right_arrow_white-icon.svg" alt=""></button>
        </div>
    </div>
</div>

<hr class="hr-devider">

<?php include_once 'includes/footer.php'; ?>