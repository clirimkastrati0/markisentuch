<?php include_once 'includes/header.php'; ?>

<div class="header2">
    <div class="overlay">
        <div class="container">
            <div class="inner_text">
                <h1>DSGVO</h1>
            </div>
        </div>
    </div>
</div>

<div class="container pt-3  agb-who-is-markisentuch">
    <span class="hr-title"></span> <span class="who-are-markisentuch">DSGVO</span>
</div>

<div class="dsgvo">
    <div class="container">
        Transparente Information zur Verarbeitung Ihrer persönlichen Daten nach Artikel 13 DSGVO stellen wir Ihnen
        gerne als PDF hier bereit:

        <div class="row pt-3">
            <div class="col-sm-12 col-lg-6">
                <div class="dsgvo-box px-5 pt-3 pb-1 mb-4">
                    <p>Interessenten, Kunden und Lieferanten:</p>
                    <p><a href="">Transparente_Information_nach_Artikel13_DSGVO</a></p>
                </div>
                <div class="dsgvo-box px-5 pt-3 pb-1 mb-4">
                    <p>Bewerber:</p>
                    <p><a href="">Transparente_Information_nach_Artikel13_DSGVO_Bewerber</a></p>
                </div>
            </div>
        </div>
    </div>
</div>

<hr class="hr-devider">

<div class="container py-2">
    <div class="row last-row">
        <div class="col-12 col-sm-12 col-lg-10">
            <h3 class="subheader-text">Sie sind <span>Fachhändler</span> und an unseren <span>Produkten</span>
                interessiert?</h3>
            <p>Kontaktieren Sie uns, wir beraten Sie gerne:</p>
        </div>
        <div class="col-12 col-sm-12 col-lg-2">
            <button class="leaders-right-about-button">Kontaktiere uns<img src="assets/icons/right_arrow_white-icon.svg" alt=""></button>
        </div>
    </div>
</div>

<hr class="hr-devider">

<?php include_once 'includes/footer.php'; ?>