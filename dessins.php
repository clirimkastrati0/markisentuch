<?php include_once 'includes/header.php'; ?>

<div class="header2 dessins-header2">
    <div class="overlay">
        <div class="container">
            <div class="inner_text dessins-inner-text">
                <h1>Dessins</h1>
                <p>Hier finden Sie einen Überblick über unser Lieferprogramm für Fachhändler. Kollektionen und
                    Preislisten senden wir Fachhändlern auf Anfrage gerne zu.</p>
                <p>Hinweis: Die Darstellung am Bildschirm kann vom Original abweichen und ist nicht verbindlich</p>
            </div>
        </div>
    </div>
</div>

<div class="filter-section bg-white">
    <div class="container">
        <div class="filter-box">
            <div class="active dropdown dropdown-container">
                <a class="dropdown-acryl-btn dropdown-toggle" type="button">
                    Acryl
                </a>
                <ul class="acryl-dropdown dropdown-menu" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="#">Acryl 1</a></li>
                    <li><a class="dropdown-item" href="#">Acryl 2</a></li>
                    <li><a class="dropdown-item" href="#">Acryl 3</a></li>
                </ul>
            </div>
            <div class="">
                <a class="" href="#">
                    Polyester
                </a>
            </div>
            <div class="dropdown dropdown-container">
                <a class="dropdown-soltis-btn dropdown-toggle" type="button">
                    Soltis
                </a>
                <ul class="soltis-dropdown dropdown-menu" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="#">Soltis 1</a></li>
                    <li><a class="dropdown-item" href="#">Soltis 2</a></li>
                    <li><a class="dropdown-item" href="#">Soltis 3</a></li>
                </ul>
            </div>
            <div>
                <a class="" href="#">
                    Screen
                </a>
            </div>
            <div>
                <a class="" href="#">
                    Tempotest Star Screen
                </a>
            </div>
            <div>
                <a class="" href="#">
                    Sattler Twilight
                </a>
            </div>
            <div>
                <a class="" href="#">
                    VU Screen
                </a>
            </div>
            <div>
                <a class="" href="#">
                    VeoZip
                </a>
            </div>
        </div>
    </div>
</div>

<div class="colors-section py-4">
    <div class="container">
        <div class="row mb-2">
            <div class="col-sm-12 col-md-6">
                <p class="number-of-colors">
                    <span style="font-weight: bold;">75</span> Ergebnisse
                </p>
            </div>
            <div class="col-sm-12 col-md-6">
                <form class="colors-search-form mb-3" action="">
                    <div class="input-group">
                        <input type="text" class="form-control shadow-none" placeholder="Artikelnummer">
                        <div class="input-group-append">
                            <button type="submit" class="input-group-text bg-white">
                                <img src="assets/icons/search-icon.svg" alt="">
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <div class="small-color-card card w-100 mb-4 active-color">
                            <img src="assets/images/color_1_1.svg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <span class="dessins-number">315420</span>
                                <button class="add-to-merkliste-button merkelisted"><img src="assets/icons/wishlist-selected-icon.svg" alt="No Image"></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="small-color-card card w-100 mb-4">
                            <img src="assets/images/color_3_3.svg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <span class="dessins-number">315420</span>
                                <button class="add-to-merkliste-button"><img src="assets/icons/wishlist-not-selected-icon.svg" alt="No Image"></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="small-color-card card w-100 mb-4">
                            <img src="assets/images/color_2_2.svg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <span class="dessins-number">315420</span>
                                <button class="add-to-merkliste-button"><img src="assets/icons/wishlist-not-selected-icon.svg" alt="No Image"></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="small-color-card card w-100 mb-4">
                            <img src="assets/images/color_3_3.svg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <span class="dessins-number">315420</span>
                                <button class="add-to-merkliste-button merkelisted"><img src="assets/icons/wishlist-selected-icon.svg" alt="No Image"></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="small-color-card card w-100 mb-4">
                            <img src="assets/images/color_1_1.svg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <span class="dessins-number">315420</span>
                                <button class="add-to-merkliste-button"><img src="assets/icons/wishlist-not-selected-icon.svg" alt="No Image"></button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="small-color-card card w-100 mb-4">
                            <img src="assets/images/color_1_1.svg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <span class="dessins-number">315420</span>
                                <button class="add-to-merkliste-button">
                                    <img src="assets/icons/wishlist-not-selected-icon.svg" alt="No Image">
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="small-color-card card w-100 mb-4">
                            <img src="assets/images/color_3_3.svg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <span class="dessins-number">315420</span>
                                <button class="add-to-merkliste-button">
                                    <img src="assets/icons/wishlist-not-selected-icon.svg" alt="No Image">
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="small-color-card card w-100 mb-4">
                            <img src="assets/images/color_1_1.svg" class="card-img-top" alt="...">
                            <div class="card-body">
                                <span class="dessins-number">315420</span>
                                <button class="add-to-merkliste-button">
                                    <img src="assets/icons/wishlist-not-selected-icon.svg" alt="No Image">
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div class="card w-100 mb-4">
                    <div class="big-image-container">
                        <img src="assets/images/color_1.svg" class="front-image" alt="">
                        <img src="assets/images/color_3.svg" class="back-image" alt="">
                        <img src="assets/images/color_1_1.svg" class="card-img-top big-image" alt="...">
                    </div>
                    <div class="card-body px-5 pt-3 ">
                        <span class="selected-dessin-category">Acryl</span> <br>
                        <span class="selected-dessin-number">315420</span>
                        <button class="selected-color-wishlist-button merkelisted">
                            <img src="assets/icons/wishlist-selected-big-icon.svg" alt="No Image">
                        </button>
                        <hr class="hr-devider">
                        <span class="download-header">Download</span>
                        <a href="" class="download-all-link">Download All</a>
                        <hr class="hr-devider">
                        <div class="py-2">
                            <p class="dessins-file">
                                <img src="assets/icons/download-icon.svg" class="mr-2" alt="">
                                <a href="">
                                    Technische Daten
                                </a>
                                <span>(PDF)</span>
                            </p>
                            <p class="dessins-file">
                                <img src="assets/icons/download-icon.svg" class="mr-2" alt="">
                                <a href="">
                                    Lichttechnische Parameter
                                </a>
                                <span>(PDF)</span>
                            </p>
                            <p class="dessins-file">
                                <img src="assets/icons/download-icon.svg" class="mr-2" alt="">
                                <a href="">
                                    REACH-Zertifikat
                                </a>
                                <span>(PDF)</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<hr class="hr-devider">

<?php include_once 'includes/footer.php'; ?>