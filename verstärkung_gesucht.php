<?php include_once 'includes/header.php'; ?>

<div class="header2">
    <div class="overlay">
        <div class="container">
            <div class="inner_text">
                <h1>Verstärkung gesucht</h1>
            </div>
        </div>
    </div>
</div>

<div class="video-section container pt-3">
    <video controls>
        <source src="mov_bbb.mp4" type="video/mp4">
        <source src="mov_bbb.ogg" type="video/ogg">
        Your browser does not support HTML video.
    </video>
</div>

<div class="verstarkung_gesucht pt-2">
    <div class="container pb-4">
        Geben wir uns gegenseitig eine Chance?
        <br><br>
        Ausbildung zum Technischen Konfektionär!
        <br><br>
        Deine Schulbildung ist uns nicht wichtig, es ist uns egal, ob Du einen Führerschein hast und welche Sprache
        Du sprichst.
        <br><br>
        Klingt verrückt? Vielleicht.
        <br><br>
        Aber wir sind nun mal davon überzeugt, dass andere Werte wichtiger sind:
        <br><br>
        Motivation und Freude an der Arbeit
        Pünktlichkeit und Zuverlässigkeit
        Teamfähigkeit und eine weltoffene Einstellung
        Bei uns arbeiten derzeit Menschen aus insgesamt 10 verschiedenen Ländern zusammen, wir sprechen Deutsch,
        Kölsch, Italienisch, Spanisch, Französisch, Arabisch, Kurdisch, Polnisch, Slowakisch, Farsi, Dari und
        fließend mit den Händen.
        <br><br>
        Wir legen Wert darauf, dass unser Team zusammenpasst und sich gegenseitig unterstützt und respektiert. Denn
        wir bilden aus, damit Du auch morgen noch dazu gehörst.
        <br><br>
        Unsere Perspektive ist langfristig und endet definitiv nicht mit dem Abschluss der Ausbildung.
        <br><br>
        Du hast Pläne? Möchtest irgendwann selber ausbilden? Ein Team leiten? Den Meister machen?
        <br><br>
        Dann ist die Ausbildung zum Technischen Konfektionär eine solide Grundlage.
        <br><br>
        Was bieten wir Dir?
        <br><br>
        Eine Ausbildung in einem erfahrenen Team mit einem modernen Maschinenpark, wir fertigen hochwertige
        Spitzenprodukte für den europäischen Markt und weltweit.
        <br><br>
        Schon während der Ausbildung unterstützen wir Dich beim Aufbau eine Altersvorsorge, bieten eine betriebliche
        Zusatzversicherung für Zahnersatz und eine angemessene Entlohnung.
        <br><br>
        Was solltest Du unbedingt mitbringen?
        <br><br>
        Interesse am Handwerk, Lust zu arbeiten, Zuverlässigkeit und Teamfähigkeit.
        <br><br>
        Alles andere erarbeiten wir gemeinsam.
        <br><br>
        Und nun?
        <br><br>
        Du hast Fragen oder möchtest Dich bewerben?
        <br><br>
        Schick uns gerne eine Mail an info@markisentuch.com!
        <br><br>
        Wir freuen uns auf Dich!
    </div>
</div>

<hr class="hr-devider">

<div class="container py-2">
    <div class="row last-row">
        <div class="col-12 col-sm-12 col-lg-10">
            <h3 class="subheader-text">Sie sind <span>Fachhändler</span> und an unseren <span>Produkten</span>
                interessiert?</h3>
            <p>Kontaktieren Sie uns, wir beraten Sie gerne:</p>
        </div>
        <div class="col-12 col-sm-12 col-lg-2">
            <button class="leaders-right-about-button">Kontaktiere uns<img src="assets/icons/right_arrow_white-icon.svg" alt=""></button>
        </div>
    </div>
</div>

<hr class="hr-devider">

<?php include_once 'includes/footer.php'; ?>